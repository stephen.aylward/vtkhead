cmake_minimum_required(VERSION 3.3 FATAL_ERROR)


PROJECT (head)
find_package(VTK REQUIRED)
vtk_module_config(VTK
  vtkCommonCore
  vtkCommonDataModel
  vtkFiltersGeneral
  vtkIOImage
  vtkIOXML
  vtkInteractionStyle
  vtkRenderingOpenGL2
)
include(${VTK_USE_FILE})

add_executable(testhead MACOSX_BUNDLE testhead.cxx)
target_link_libraries(testhead ${VTK_LIBRARIES})
